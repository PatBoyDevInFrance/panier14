<?php

namespace App\Form;

use App\Entity\Recherche;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechercheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('minPrix',IntegerType::class, [
                'required' => false,
                'label' => 'Prix minimum',
                'attr' => [
                    'placeholder' => 'Prix min'
                ]
            ])
            ->add('maxPrix',IntegerType::class, [
                'required' => false,
                'label' => 'Prix maximum',
                'attr' => [
                    'placeholder' => 'Prix max'
                ]
            ])
            ->add('submit',SubmitType::class,[
                'label' => 'rechercher',
                'attr' => [
                    'class' => 'btn btn-info'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recherche::class,
        ]);
    }
}
