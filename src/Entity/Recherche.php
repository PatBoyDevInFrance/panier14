<?php

namespace App\Entity;

class Recherche{

    private $minPrix;
    private $maxPrix;

    public function getMinPrix(){return $this->minPrix;}
    public function setMinPrix(int $prix){$this->minPrix = $prix; return $this;}

    public function getMaxPrix(){return $this->maxPrix;}
    public function setMaxPrix(int $prix){$this->maxPrix = $prix; return $this;}
}