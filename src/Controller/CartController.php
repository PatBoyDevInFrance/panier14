<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Recherche;
use App\Form\RechercheType;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     */
    public function index(Cart $carte,ProductRepository $repo,PaginatorInterface $paginatorInterface, Request $request): Response
    {
        $recherche = new Recherche;
        $form = $this->createForm(RechercheType::class, $recherche);
        $form->handleRequest($request);

        $cart = $paginatorInterface->paginate(
            $repo->findAllWithPaginator($recherche), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );

        return $this->render('cart/cart.html.twig', [
            'cartes' => $cart,
            'form' => $form->createView(),  
            'admin' => false
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="panier_add" )
     */
    public function add($id, Cart $cart): Response
    {       
        $cart->add($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/decrease/{id}", name="decrease_to_cart")
     */
    public function decrease ($id, Cart $cart): Response
    {       
        $cart->decrease($id);

        return $this->redirectToRoute('cart');
    }

}



