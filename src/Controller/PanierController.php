<?php

namespace App\Controller;

use App\Classe\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier_index")
     */
    public function index(Cart $carte): Response
    {
        return $this->render('panier/index.html.twig',[
            'items' => $carte->getFullCart(),
            'total'=> $carte->getTotal(),
            'totalQ'=> $carte->getQuantity(),
        ]);
    }


    /**
     * @Route("/panier/add/{id}" , name="panier_add")
     */
    
    public function add($id, Cart $cart): Response
    {       
        $cart->add($id);

        return $this->redirectToRoute('cart');
    }

    /**
     * @Route("/cart/decrease/{id}", name="decrease_to_cart")
     */
    public function decrease ($id, Cart $cart): Response
    {       
        $cart->decrease($id);

        return $this->redirectToRoute('cart');
    }

   
}

    
