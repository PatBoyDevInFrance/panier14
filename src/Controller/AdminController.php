<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Recherche;
use App\Form\AdminType;
use App\Form\RechercheType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function index(Request $request, PaginatorInterface $paginatorInterface, ProductRepository $repo): Response
    {
        $recherche = new Recherche;
        $form = $this->createForm(RechercheType::class, $recherche);
        $form->handleRequest($request); 

        $cart = $paginatorInterface->paginate(
            $repo->findAllWithPaginator($recherche), /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            6 /*limit per page*/
        );
        return $this->render('cart/cart.html.twig', [
            'cartes' => $cart,
            'form' => $form->createView(),
            'admin' => true
        ]);
    }

    /**
     * @Route("/admin/creation", name="creationBien")
     * @Route("/admin/{id}", name="admin_modif")
     */
    public function modif(Product $product, EntityManagerInterface $em, Request $request): Response
    {
        $form = $this->createForm(AdminType::class, $product );
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('admin_index');
        }
        return $this->render('admin/modif.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
           
        ]);


    }
   
}
